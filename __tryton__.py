# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'IR Sequence Log',
    'name_de_DE': 'Interne Administration Nummernkreis Protokoll',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
        Provides functionality to log all requested numbers from sequences.
        The sequences to log are specified by the user.
    ''',
    'description_de_DE': '''
        Stellt Funktionalität zur Verfügung, um ein Protokoll über angefragte
        Nummern von Nummernkreisen zu führen.
        Nummernkreise die protokolliert werden sollen, können vom Benutzer
        festgelegt werden.
    ''',
    'depends': [
        'ir',
        'res',
    ],
    'xml': [
        'ir.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
