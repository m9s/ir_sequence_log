# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.config import CONFIG


SEQUENCE_REQUESTER = CONFIG.get('sequence_requester', 'internal')


class SequenceLog(ModelSQL, ModelView):
    'Sequence Log'
    _name = 'ir.sequence.log'
    _description = __doc__

    requester = fields.Char('Requester', required=True, readonly=True)
    number = fields.Char('Number', required=True, readonly=True)
    sequence = fields.Many2One('ir.sequence', 'Sequence', readonly=True)
    sequence_strict = fields.Many2One('ir.sequence.strict', 'Strict Sequence',
        readonly=True)

    def __init__(self):
        super(SequenceLog, self).__init__()
        self._sql_constraints += [
            ('ir_sequence_log_number_sequence_uniq',
                'UNIQUE (number, sequence)',
                'Number must be unique by sequence.'),
            ('ir_sequence_log_number_sequence_strict_uniq',
                'UNIQUE (number, sequence_strict)',
                'Number must be unique by strict sequence.'),
        ]
        self._order.insert(0, ('id', 'DESC'))

SequenceLog()


class Sequence(ModelSQL, ModelView):
    _name = 'ir.sequence'

    log = fields.Boolean('Log')

    def get_id(self, domain):
        pool = Pool()
        sequence_log_obj = pool.get('ir.sequence.log')

        result = super(Sequence, self).get_id(domain)

        if isinstance(domain, (int, long)):
            domain = [('id', '=', domain)]

        with Transaction().set_user(0):
            sequence_id, = self.search(domain, limit=1)
            if sequence_id:
                sequence = self.browse(sequence_id)
                if sequence.log:
                    sequence_strict_id = None
                    if self._strict:
                         sequence_strict_id, sequence_id = sequence_id, None
                    sequence_log_entry = self._seq_log_entry_map({
                            'sequence_id': sequence_id,
                            'sequence_strict_id': sequence_strict_id,
                            'number': result,
                            'requester': SEQUENCE_REQUESTER,
                            })
                    sequence_log_obj.create(sequence_log_entry)

        return result

    def _seq_log_entry_map(self, values):
        seq_log_entry = {
            'sequence': values.get('sequence_id'),
            'sequence_strict': values.get('sequence_strict_id'),
            'number': values.get('number'),
            'requester': values.get('requester'),
            }
        return seq_log_entry

Sequence()


class SequenceStrict(Sequence):
    _name = 'ir.sequence.strict'

SequenceStrict()
