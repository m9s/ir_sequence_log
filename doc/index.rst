Sequence Log
============
:Model Name: ``ir.sequence, ir.sequence.strict``
:Base Module: ``ir_sequence_log``

:Menu:
    :menuselection:`Administration --> Sequences --> Sequence Log`


The *Sequence Log*-model introduces the conception of a log-book of used
sequence numbers for selected sequences. Logging of sequences is activated by
field ``Log`` in models ``ir_sequence`` and ``ir_sequence_strict``.
The model additionally forces the unique use of sequence numbers per
sequence. A new sequence number is not created, if it would be the same as an
already logged sequence number.


Configuration options
---------------------
The *Sequence Log*-model uses the following configuration options in
``trytond.conf``:

sequence_requester:
    Name to identify the requester. This name is shown in model
    *ir.sequence.log*, field *Requester* log entries.
    The default value is "internal".


Fields
------
:guilabel:`Requester`:
    :Internal Name: ``requester``
    :Type: char

    The requester for the sequence.

:guilabel:`Number`:
    :Internal Name: ``number``
    :Type: char

    The number or value of the sequence.

:guilabel:`Sequence`:
    :Internal Name: ``sequence``
    :Type: many2one
    :Relation Model: ``ir_sequence``

    The Sequence (``ir_sequence``), if not a Strict Sequence
    (``ir_sequence_strict``) is used.

:guilabel:`Sequence Strict`:
    :Internal Name: ``sequence_strict``
    :Type: many2one
    :Relation Model: ``ir_sequence_strict``

    The Strict Sequence (``ir_sequence_strict``), if not a Sequence
    (``ir_sequence``) is used.


Sequence
========
:Model Name: ``ir.sequence``


Fields
------
:guilabel:`Log`:
    :Internal Name: ``log``
    :Type: boolean
    :Base_module: ``ir_sequence_log``

    If ticked, the use of this sequence is logged in model `ir_sequence_log`.
